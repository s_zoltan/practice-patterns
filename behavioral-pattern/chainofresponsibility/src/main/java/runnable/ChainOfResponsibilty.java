package main.java.runnable;

import main.java.model.Director;
import main.java.model.Manager;
import main.java.model.Supervisor;
import main.java.services.Employee;

public class ChainOfResponsibilty {
    public static void main(String[] args) {
        Employee director = new Director();
        Employee manager = new Manager();
        Employee supervisor = new Supervisor();

        supervisor.setNext(manager);
        manager.setNext(director);

        supervisor.request(11900);
        supervisor.request(1900);
        supervisor.request(900);
        supervisor.request(900000);


    }
}
