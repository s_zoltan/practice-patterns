package main.java.model;

import main.java.services.Employee;

public class Manager extends Employee {

    public Manager() {
        position = "Manager";
        authorization = 10000;
    }

}
