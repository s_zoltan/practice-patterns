package main.java.model;

import main.java.services.Employee;

public class Supervisor extends Employee {

    public Supervisor() {
        position = "Supervisor";
        authorization = 1000;
    }
}
