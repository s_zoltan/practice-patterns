package main.java.services;

public abstract class Employee {
    protected String position;
    protected Employee next;
    protected int authorization;

    public void request(int request) {
        if (request <= authorization) {
            System.out.println("Request of "+request+" authorized by "+position);
        }
        else if (next != null) {
            next.request(request);
        }
        else System.out.println("Request of " +request+" denied");
    }

    public void setNext(Employee next) {
        this.next = next;
    }
}
