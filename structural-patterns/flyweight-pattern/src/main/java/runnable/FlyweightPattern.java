package main.java.runnable;

import main.java.model.Order;
import main.java.tools.CarFactory;

public class FlyweightPattern {
    public static void main(String[] args) {
        Order orders = new Order();

        orders.addCar("Hungary","AAA auto","JLR","Defender","Black");
        orders.addCar("Italy","SA.Car","JLR","Defender","Black");
        orders.addCar("Hungary","Super","Toyota","Corolla","Black");
        orders.addCar("Hungary","Super","Toyota","Corolla","Red");
        orders.addCar("Germany","Autostar","Toyota","Corolla","Red");
        orders.addCar("Germany","Autostar","JLR","Defender","Black");

        orders.listOrders();

        CarFactory.listTypes();


    }
}
