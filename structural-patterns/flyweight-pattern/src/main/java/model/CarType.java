package main.java.model;

public class CarType {
    private String oem;
    private String vehicle;
    private String color;

    public CarType(String oem, String vehicle, String color) {
        this.oem = oem;
        this.vehicle = vehicle;
        this.color = color;
    }

    public String getOem() {
        return oem;
    }

    public String getVehicle() {
        return vehicle;
    }

    public String getColor() {
        return color;
    }
}
