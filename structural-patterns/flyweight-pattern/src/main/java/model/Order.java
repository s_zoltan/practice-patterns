package main.java.model;

import main.java.tools.CarFactory;

import java.util.ArrayList;


public class Order {
    private ArrayList<Car> cars = new ArrayList<>();

    public void addCar(String country, String distributor, String oem, String vehicle, String color ) {
        CarType type = CarFactory.getCarType(oem,vehicle,color);
        Car car = new Car(country,distributor,type);
        cars.add(car);
    }

    public void listOrders() {
        for (Car car: cars ) {
            System.out.print("Order request to "+car.getCountry()+ ", "+car.getDistributor()+" : ");
            System.out.println(car.getCarType().getOem()+" "+car.getCarType().getVehicle()+" in "+car.getCarType().getColor()+" color.");

        }

    }
}
