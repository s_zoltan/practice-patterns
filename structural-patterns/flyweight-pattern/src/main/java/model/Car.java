package main.java.model;

public class Car {
    private String country;
    private String distributor;
    private CarType carType;

    public Car(String country, String distributor, CarType carType) {
        this.country = country;
        this.distributor = distributor;
        this.carType = carType;
    }

    public String getCountry() {
        return country;
    }

    public String getDistributor() {
        return distributor;
    }

    public CarType getCarType() {
        return carType;
    }
}
