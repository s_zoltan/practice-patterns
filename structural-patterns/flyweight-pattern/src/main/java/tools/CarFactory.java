package main.java.tools;

import main.java.model.CarType;

import java.util.HashMap;


public class CarFactory {
    static HashMap<String, CarType> carTypes = new HashMap<>();

    public static CarType getCarType(String oem,String vehicle, String color) {
        CarType carType = carTypes.get(oem+vehicle+color);
        if (carType == null) {
            carType = new CarType(oem,vehicle,color);
            carTypes.put(oem+vehicle+color, carType);
        }
        return carType;
    }

    public static void listTypes() {
        System.out.println("\nNumber of types: "+carTypes.size()+ " "+carTypes);

    }
}
