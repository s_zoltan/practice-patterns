package main.java.services;

public interface Component {
    float getPrice();
}
