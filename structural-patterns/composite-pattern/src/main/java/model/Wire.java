package main.java.model;

import main.java.services.Component;

public class Wire implements Component {
    private float price;

    public Wire(float price) {
        this.price = price;
    }

    @Override
    public float getPrice() {
        return price;
    }
}
