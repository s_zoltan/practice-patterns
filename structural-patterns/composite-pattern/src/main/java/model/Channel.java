package main.java.model;

import main.java.services.Component;

public class Channel implements Component {
    private float price;

    public Channel(float price) {
        this.price = price;
    }

    @Override
    public float getPrice() {
        return price;
    }
}
