package main.java.model;

import main.java.services.Component;

import java.util.ArrayList;



public class Harness implements Component {
    private float price;

    ArrayList<Component> componentlist = new ArrayList<>();

    public void Add(Component component) {
        componentlist.add(component);
    }

    @Override
    public float getPrice() {
        price = 0;
        for (Component component:componentlist) {
            price += component.getPrice();
        }
        return price;
    }
}
