package main.java.model;

import main.java.services.Component;

public class Seal implements Component {
    private float price;

    public Seal(float price) {
        this.price = price;
    }

    @Override
    public float getPrice() {
        return price;
    }
}
