package main.java.model;

import main.java.services.Component;

public class Housing implements Component {
    private float price;

    public Housing(float price) {
        this.price = price;
    }

    @Override
    public float getPrice() {
        return price;
    }
}
