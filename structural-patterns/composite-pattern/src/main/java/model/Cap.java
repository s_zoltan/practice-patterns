package main.java.model;

import main.java.services.Component;

public class Cap implements Component {
    private float price;

    public Cap(float price) {
        this.price = price;
    }

    @Override
    public float getPrice() {
        return price;
    }
}
