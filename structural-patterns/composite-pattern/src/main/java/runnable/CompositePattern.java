package main.java.runnable;

import main.java.model.*;

public class CompositePattern {
    public static void main(String[] args) {
        Wire wire = new Wire(10);
        Channel channel = new Channel(3);
        Seal seal = new Seal(0.5f);
        Housing housing = new Housing(1.5f);
        Cap cap = new Cap(0.5f);

        Connector connector = new Connector();
        connector.Add(housing);
        connector.Add(seal);
        connector.Add(cap);

        Harness harness = new Harness();

        harness.Add(wire);
        harness.Add(connector);
        harness.Add(connector);
        harness.Add(channel);


        float totalPrice = harness.getPrice();
        System.out.println("Total price is "+totalPrice);




    }
}
