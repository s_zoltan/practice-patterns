package main.java.services;

import main.java.model.Computer;
import main.java.model.Monitor;
import main.java.model.Printer;
import main.java.model.Software;

public class FacadePrint {
    private Computer computer;
    private Monitor monitor;
    private Printer printer;
    private Software software;

    public FacadePrint(Computer computer, Monitor monitor, Printer printer, Software software) {
        this.computer = computer;
        this.monitor = monitor;
        this.printer = printer;
        this.software = software;
    }

    public FacadePrint() {
        this.computer = new Computer();
        this.monitor = new Monitor();
        this.printer = new Printer();
        this.software = new Software(computer,monitor,printer);
    }

    public void Print() {
        computer.plugIn();
        computer.turnOn();
        monitor.plugIn();
        monitor.turnOn();
        printer.plugIn();
        printer.turnOn();
        software.launch();
        software.print();
    }
}
