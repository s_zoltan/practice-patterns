package main.java.model;

public class Software {
    private boolean running;
    private Computer computer;
    private Monitor monitor;
    private Printer printer;

    public Software(Computer computer, Monitor monitor, Printer printer) {
        this.computer = computer;
        this.monitor = monitor;
        this.printer = printer;
    }

    public void launch() {
        running = true;
    }

    public void shutdown() {
        running = false;
    }

    public void print() {
        if (running && computer.isTurnedOn() && monitor.isTurnedOn() && printer.isTurnedOn()) {
            try {
                printer.print();
            }
            catch (InterruptedException e) {
                System.out.println("Printing aborted.");
            }
        }
        else System.out.println("Turn on all hardware.");
    }
}
