package main.java.model;

public class Computer extends Hardware {
    private boolean cdTrayOpened = false;

    public void reset() {
        turnedOn = false;
        turnedOn = true;
    }

    public void ejectCd() {
        if (cdTrayOpened) cdTrayOpened = false;
        else cdTrayOpened = true;
    }

}
