package main.java.model;

public abstract class Hardware {
    protected boolean turnedOn;
    protected boolean pluggedIn;

    public void plugIn() {
        pluggedIn = true;
    }

    public void plugOut() {
        pluggedIn = false;
        turnedOn = false;
    }

    public void turnOn() {
        if (pluggedIn) turnedOn = true;
        else System.out.println("Plug in first");
    }

    public void turnOff() {
        turnedOn = false;
    }

    public boolean isTurnedOn() {
        return turnedOn;
    }

    public boolean isPluggedIn() {
        return pluggedIn;
    }

}
