package main.java.model;

import java.util.concurrent.TimeUnit;

public class Printer extends Hardware {

    public void print() throws InterruptedException {
        System.out.println("Printing...");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(".");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(".");
        TimeUnit.SECONDS.sleep(1);
        System.out.println(".");
        TimeUnit.SECONDS.sleep(1);
        System.out.println("Printing completed");
    }
}
