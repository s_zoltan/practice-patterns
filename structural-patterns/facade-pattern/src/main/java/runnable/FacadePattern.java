package main.java.runnable;

import main.java.services.FacadePrint;

public class FacadePattern {
    public static void main(String[] args) {
        FacadePrint facadeTest = new FacadePrint();
        facadeTest.Print();
    }
}
