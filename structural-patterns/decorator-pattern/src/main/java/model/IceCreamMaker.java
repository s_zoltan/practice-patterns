package main.java.model;

import main.java.services.KitchenMachine;
import main.java.services.KitchenMachineAssembly;

public class IceCreamMaker extends KitchenMachineAssembly {
    public IceCreamMaker(KitchenMachine kitchenMachine) {
        super(kitchenMachine);
    }

    @Override
    public String assemble() {
        return super.assemble() + " with ice cream maker";
    }
}
