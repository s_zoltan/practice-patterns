package main.java.model;

import main.java.services.KitchenMachine;

public class KitchenMachineImpl implements KitchenMachine {
    @Override
    public String assemble() {
        return "Kitchen Machine assembled";
    }
}
