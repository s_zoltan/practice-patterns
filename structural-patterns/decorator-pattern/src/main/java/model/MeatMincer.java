package main.java.model;

import main.java.services.KitchenMachine;
import main.java.services.KitchenMachineAssembly;

public class MeatMincer extends KitchenMachineAssembly {

    public MeatMincer(KitchenMachine kitchenMachine) {
        super(kitchenMachine);
    }

    @Override
    public String assemble() {
        return super.assemble()+" with meat mincer";
    }
}
