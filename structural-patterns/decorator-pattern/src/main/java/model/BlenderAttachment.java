package main.java.model;

import main.java.services.KitchenMachine;
import main.java.services.KitchenMachineAssembly;

public class BlenderAttachment extends KitchenMachineAssembly {

    public BlenderAttachment(KitchenMachine kitchenMachine) {
        super(kitchenMachine);
    }

    public String assemble() {
        return super.assemble()+" with blender attachment";
    }
}
