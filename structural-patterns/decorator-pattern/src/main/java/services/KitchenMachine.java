package main.java.services;

public interface KitchenMachine {
    String assemble();
}
