package main.java.services;

public abstract class KitchenMachineAssembly implements KitchenMachine {
    private KitchenMachine kitchenMachine;

    public KitchenMachineAssembly(KitchenMachine kitchenMachine) {
        this.kitchenMachine = kitchenMachine;
    }

    @Override
    public String assemble() {
        return kitchenMachine.assemble();
    }
}
