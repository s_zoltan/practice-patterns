package main.java.runnable;

import main.java.model.BlenderAttachment;
import main.java.model.IceCreamMaker;
import main.java.model.KitchenMachineImpl;
import main.java.model.MeatMincer;
import main.java.services.KitchenMachine;

import javax.swing.plaf.basic.BasicLookAndFeel;

public class DecoratorPattern {
    public static void main(String[] args) {
        KitchenMachine machine = new KitchenMachineImpl();
        System.out.println(machine.assemble());

        KitchenMachine machine2 = new IceCreamMaker(new MeatMincer(new BlenderAttachment(new KitchenMachineImpl())));
        System.out.println(machine2.assemble());

    }
}
