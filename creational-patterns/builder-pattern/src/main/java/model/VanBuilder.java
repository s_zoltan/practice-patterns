package main.java.model;

import main.java.services.Car;
import main.java.services.CarBuilder;

public class VanBuilder implements CarBuilder {
    private Car car;

    public VanBuilder() {this.car = new Car();}

    @Override
    public void buildEngine() {
        car.setEngine("Diesel");
    }

    @Override
    public void buildTransmission() {
        car.setTransmission("Automatic");
    }

    @Override
    public void buildType() {
        car.setType("Minivan");
    }

    @Override
    public Car getCar() {
        return car;
    }
}
