package main.java.model;

import main.java.services.Car;
import main.java.services.CarBuilder;
import main.java.tools.*;


public class CustomizedBuilder implements CarBuilder {


    private Car car;
    private String engine;
    private String transmission;
    private String type;

    public CustomizedBuilder(engineList engine, transmissionList transmission, typeList type) {
        this.car = new Car();
        this.engine = engine.toString();
        this.transmission = transmission.toString();
        this.type = type.toString();
    }

    @Override
    public void buildEngine() {
        car.setEngine(engine);
    }

    @Override
    public void buildTransmission() {
        car.setTransmission(transmission);
    }

    @Override
    public void buildType() {
        car.setType(type);
    }

    @Override
    public Car getCar() {
        return car;
    }

}
