package main.java.model;

import main.java.services.Car;
import main.java.services.CarBuilder;

public class SportCarBuilder implements CarBuilder {
    private Car car;

    public SportCarBuilder() {this.car = new Car();}

    @Override
    public void buildEngine() {
        car.setEngine("Petrol");
    }

    @Override
    public void buildTransmission() {
        car.setTransmission("Manual");
    }

    @Override
    public void buildType() {
        car.setType("Coupe");
    }

    @Override
    public Car getCar() {
        return car;
    }
}
