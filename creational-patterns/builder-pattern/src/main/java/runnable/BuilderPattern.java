package main.java.runnable;

import main.java.model.CustomizedBuilder;
import main.java.model.SportCarBuilder;
import main.java.model.VanBuilder;
import main.java.services.Car;
import main.java.services.CarBuilder;
import main.java.services.CarDesigner;
import main.java.tools.*;


import java.util.HashSet;
import java.util.Set;

public class BuilderPattern {
    public static void main(String[] args) {

        Set<Car> carList = new HashSet<>();

        CarBuilder sportCarBuilder = new SportCarBuilder();
        CarBuilder vanBuilder = new VanBuilder();


        CarDesigner designer = new CarDesigner(sportCarBuilder);
        designer.constructCar();
        Car sportCar = designer.getCar();
        carList.add(sportCar);

        designer = new CarDesigner(vanBuilder);
        designer.constructCar();
        Car van = designer.getCar();
        carList.add(van);

        CarBuilder electricBuilder = new CustomizedBuilder(engineList.Electric, transmissionList.Automatic, typeList.Sedan);
        designer = new CarDesigner(electricBuilder);
        designer.constructCar();
        Car electricCar = designer.getCar();
        carList.add(electricCar);

        for (Car car: carList) {
            System.out.println("You built a "+car.getType()+ " with "+car.getEngine()+" engine and "+car.getTransmission()+" transmission.");
        }


    }
}
