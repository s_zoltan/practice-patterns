package main.java.services;

public interface CarBuilder {
    void buildType();
    void buildEngine();
    void buildTransmission();
    Car getCar();
}
