package main.java.services;

public class CarDesigner {
    private CarBuilder carBuilder;
    public CarDesigner(CarBuilder carBuilder) {
        this.carBuilder = carBuilder;
    }

    public Car getCar() {
        return this.carBuilder.getCar();
    }

    public void constructCar() {
        this.carBuilder.buildEngine();
        this.carBuilder.buildTransmission();
        this.carBuilder.buildType();
    }
}

