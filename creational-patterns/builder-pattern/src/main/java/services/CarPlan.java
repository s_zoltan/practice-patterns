package main.java.services;

public interface CarPlan {
    void setType(String type);
    void setEngine(String engine);
    void setTransmission(String transmission);
}
