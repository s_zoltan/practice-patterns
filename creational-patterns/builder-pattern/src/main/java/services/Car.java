package main.java.services;

public class Car implements CarPlan{
    private String engine;
    private String type;
    private String transmission;


    @Override
    public void setEngine(String engine) {
        this.engine = engine;
    }

    @Override
    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    public String getEngine() {
        return engine;
    }
    public String getType() {
        return type;
    }

    public String getTransmission() {
        return transmission;
    }
}
