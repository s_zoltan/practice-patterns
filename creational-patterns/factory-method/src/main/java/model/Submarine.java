package main.java.model;

import main.java.services.AttackVehicle;

public class Submarine implements AttackVehicle {



    @Override
    public void selectWeapon() {
        System.out.println("Submarine selects torpedo");
    }

    @Override
    public void attack() {
        System.out.println("Commence torpedo attack");
    }

    @Override
    public void fallback() {
        System.out.println("Submerge to safety");
    }
}
