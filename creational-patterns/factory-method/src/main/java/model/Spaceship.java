package main.java.model;

import main.java.services.AttackVehicle;

public class Spaceship implements AttackVehicle {
    @Override
    public void selectWeapon() {
        System.out.println("Spaceship selects laser");
    }

    @Override
    public void attack() {
        System.out.println("Shoot lasers");
    }

    @Override
    public void fallback() {
        System.out.println("Warps away");
    }
}
