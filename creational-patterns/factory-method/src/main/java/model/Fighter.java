package main.java.model;

import main.java.services.AttackVehicle;

public class Fighter implements AttackVehicle {
    @Override
    public void selectWeapon() {
        System.out.println("Fighter selects missile");
    }

    @Override
    public void attack() {
        System.out.println("Launch missile attack");
    }

    @Override
    public void fallback() {
        System.out.println("Flies back to base");
    }
}
