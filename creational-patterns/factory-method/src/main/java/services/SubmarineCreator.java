package main.java.services;

import main.java.model.*;


public class SubmarineCreator extends Creator {

    @Override
    AttackVehicle createAttackVehicle() {
        return new Submarine();
    }
}
