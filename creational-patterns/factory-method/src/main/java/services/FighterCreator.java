package main.java.services;

import main.java.model.*;


public class FighterCreator extends Creator{

    @Override
    AttackVehicle createAttackVehicle() {
        return new Fighter();
    }
}
