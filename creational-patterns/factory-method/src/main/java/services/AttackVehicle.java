package main.java.services;

public interface AttackVehicle {
    void selectWeapon();
    void attack();
    void fallback();
}
