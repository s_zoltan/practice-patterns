package main.java.services;

public abstract class Creator {
    abstract AttackVehicle createAttackVehicle();

    public void create() {
        AttackVehicle attackVehicle = createAttackVehicle();
        attackVehicle.selectWeapon();
        attackVehicle.attack();
        attackVehicle.fallback();
    }

}
