package main.java.services;

import main.java.model.*;

public class SpaceshipCreator extends Creator{
    @Override
    AttackVehicle createAttackVehicle() {
        return new Spaceship();
    }
}
