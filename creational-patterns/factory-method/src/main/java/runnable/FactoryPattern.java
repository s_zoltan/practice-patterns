package main.java.runnable;

import main.java.services.Creator;
import main.java.services.FighterCreator;
import main.java.services.SpaceshipCreator;
import main.java.services.SubmarineCreator;

public class FactoryPattern {
    private static String genre;
    private static Creator creator;


    public static void main(String[] args) {


        //genre = "Top Gun";
        //genre = "Star Trek";
        genre = "Battleship";

        configure();
        creator.create();

    }

    static void configure() {
        switch (genre) {
            case "Top Gun":
                creator = new FighterCreator();
                break;
            case "Star Trek":
                creator = new SpaceshipCreator();
                break;
            case "Battleship":
                creator = new SubmarineCreator();
                break;
        }

    }
}
